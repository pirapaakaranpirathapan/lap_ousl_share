// In App.js in a new project

import * as React from 'react';
import { View, Text,Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Signup from './src/components/Signup'
import Login from './src/components/Login'
import DashBoard from './DashBoard';
import Forgotpassword from './src/components/Forgotpassword'
function HomeScreen({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',flexDirection:'row',backgroundColor:"pink" }}>
      
      <View>
      <Button title='Login' onPress={() => navigation.navigate('Login')}/>
      <Button title='Register' onPress={() => navigation.navigate('Register')}/>
      </View>

      <View>
      <Button title='DashBoard' onPress={() => navigation.navigate('Dashboard')}/>
      </View>

      <Text onPress={() => navigation.navigate('Forgot')}> Forgot Password </Text>
    </View>
  );
}



const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Signup} />
        <Stack.Screen name="Dashboard" component={DashBoard} />
        <Stack.Screen name="Forgot" component={Forgotpassword} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;