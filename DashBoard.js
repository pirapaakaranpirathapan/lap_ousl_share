import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import { Searchbar, ProgressBar, Colors } from 'react-native-paper';
import Booking from './src/components/Booking'
import Notification from './src/components/Notification'
const DashBoard = () => {
    return (
        <View>
            <ScrollView>
            <Searchbar
                placeholder="Search"

            />
            <View style={styles.body}>
                <Text style={styles.title}>Fuel::</Text>
                <Text>Petrol:80%</Text>
                <ProgressBar progress={0.8} color={Colors.red800} />

                <Text>Diesel 70%</Text>
                <ProgressBar progress={0.7} color={Colors.red800} />

                <Text>Kerosine 40%</Text>
                <ProgressBar progress={0.2} color={Colors.red800} />

                <Text>LP Gas 50%</Text>
                <ProgressBar progress={0.3} color={Colors.red800} />
            </View>
            
                <Text>Notifications</Text>
                <Notification />
                <Text>BookNow</Text>
                <Booking />
            </ScrollView>
        </View>
    )
}

export default DashBoard

const styles = StyleSheet.create({
    title: {
        fontSize: 30,

    },
    body: {
        backgroundColor: 'yellow',
        padding: 10,
        margin: 10,
        borderRadius: 10
    }
})