import { StyleSheet, Text, View,ScrollView } from 'react-native'
import React from 'react'
import { TextInput, Button } from 'react-native-paper';
const Login = () => {
  return (
    <View style={styles.layout}>
     <ScrollView>
     <View style={styles.minlayout}>
      <Text>BookNow</Text>
      <TextInput
        mode="outlined"
        label="Enter Your Name"
        placeholder="Please Enter Your Name"
        right={<TextInput.Affix text="/100" />}
      />

      <TextInput
        mode="outlined"
        label="Enter Your Fuel Details"
        placeholder="Please Enter Your Fuel Details"
        right={<TextInput.Affix text="/100" />}
      />

    <TextInput
        mode="outlined"
        label="Enter Your District"
        placeholder="Please Enter Your District"
        right={<TextInput.Affix text="/100" />}
      />

<TextInput
        mode="outlined"
        label="Enter Your City"
        placeholder="Please Enter Your City"
        right={<TextInput.Affix text="/100" />}
      />

      <Button style={styles.butt} mode="contained" onPress={() => console.log('Login')}>
        Submit
      </Button>
      </View>
     </ScrollView>
    </View>
  )
}

export default Login

const styles = StyleSheet.create({
  layout: {
    backgroundColor: "pink",
    
    margin: 10,
    padding: 10,
    borderRadius: 5,
    justifyContent: 'center'
  },
  minlayout:{
    backgroundColor:"white",
    padding:10,
    borderRadius:15
  },
  butt:{
    marginTop:10
  }
})
