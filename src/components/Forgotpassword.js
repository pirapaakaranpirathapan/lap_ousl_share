import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { TextInput, Button } from 'react-native-paper';
const Login = () => {
  return (
    <View style={styles.layout}>
      <View style={styles.minlayout}>
      <Text>Forgot password</Text>
      <TextInput
        mode="outlined"
        label="Enter Your Email"
        placeholder="Please Enter Your Email"
        right={<TextInput.Affix text="/100" />}
      />

      

      <Button style={styles.butt} mode="contained" onPress={() => console.log('Login')}>
        Forgot
      </Button>
      </View>
    </View>
  )
}

export default Login

const styles = StyleSheet.create({
  layout: {
    backgroundColor: "pink",
    height: '100%',
    margin: 10,
    padding: 10,
    borderRadius: 5,
    justifyContent: 'center'
  },
  minlayout:{
    backgroundColor:"white",
    padding:10,
    borderRadius:15
  },
  butt:{
    marginTop:10
  }
})
